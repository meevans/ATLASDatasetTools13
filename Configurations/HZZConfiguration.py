Job = {
    "Batch"           : True,
    "Analysis"        : "HZZAnalysis",
    "Fraction"        : 1,
    "MaxEvents"       : 1234567890,
    "OutputDirectory" : "resultsHZZ/"
}

Processes = {

    # H -> ZZ -> 4lep processes
    "ZH125_ZZ4lep"          : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/MC/mc_341947.ZH125_ZZ4lep.4lep.root",
    "WH125_ZZ4lep"          : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/MC/mc_341964.WH125_ZZ4lep.4lep.root",
    "VBFH125_ZZ4lep"        : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/MC/mc_344235.VBFH125_ZZ4lep.4lep.root",
    "ggH125_ZZ4lep"         : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/MC/mc_345060.ggH125_ZZ4lep.4lep.root",

    # Z + jets processes
    "Zee"                   : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/MC/mc_361106.Zee.4lep.root",
    "Zmumu"                 : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/MC/mc_361107.Zmumu.4lep.root",

    # Diboson processes
    "ZqqZll"                : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/MC/mc_363356.ZqqZll.4lep.root",
    "WqqZll"                : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/MC/mc_363358.WqqZll.4lep.root",
    "llll"                  : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/MC/mc_363490.llll.4lep.root",
    "lllv"                  : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/MC/mc_363491.lllv.4lep.root",
    "llvv"                  : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/MC/mc_363492.llvv.4lep.root",

    # top pair processes
    "ttbar_lep"             : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/MC/mc_410000.ttbar_lep.4lep.root",

    # Data
    "data_A"                : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/Data/data_A.4lep.root",
    "data_B"                : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/Data/data_B.4lep.root",
    "data_C"                : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/Data/data_C.4lep.root",
    "data_D"                : "/eos/user/t/thsteven/OpenData/opentuplepostprocess/renamed/4lep/Data/data_D.4lep.root",

}
